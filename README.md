# Setup S3 and CloudFront with AWS CDK 
1. Clone this repo
2. cd into project folder 
3. `npm install`
4. Make sure that aws cli is installed
5. `npm i @aws-cdk/core @aws-cdk/aws-cloudfront @aws-cdk/aws-cloudfront-origins @aws-cdk/aws-s3`
6. `aws configure` - enter your AWS account data
7. `npm run upgrade`
8. `npm install dotenv` if you have an error, try `npm install --dotenv-extended` 
9. `npm run cdk synth` - ensure that all the updates are here and expected
10. `npm run cdk deploy`

## Useful commands

| Command         | Description                                          |
| --------------- | ---------------------------------------------------- |
| `cdk deploy`    | Deploy this stack to your default AWS account/region |
| `cdk diff`      | Compare deployed stack with current state            |
| `cdk synth`     | Emits the synthesized CloudFormation template        |
| `cdk destroy`   | Tear down your deployed stack                        |
